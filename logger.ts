import winston, { info } from 'winston'

interface ILogger{
  getDate: () => string
  getLoggerConsole: () => winston.Logger
  getLoggerFile: () => winston.Logger
}

export class Logger implements ILogger{
  public static instance: Logger | undefined = undefined

  public static getInstance(): Logger{
    if(this.instance !== undefined)
      return this.instance
    this.instance = new Logger()
    return this.instance
  }

  constructor(){}

  private readonly consoleFormat = winston.format.combine(
    winston.format.combine(
      winston.format.colorize(),
      winston.format.timestamp(),
      winston.format.align(),
      winston.format.printf(
        info => `${info.timestamp} level: ${info.level} message: ${info.message} status: ${info.status} res Time: ${info['X-Response-Time']}`
      )
    )
  )

  private readonly fileFormat = winston.format.combine(
    winston.format.json(),
    winston.format.timestamp(),
    winston.format.ms()
  )
  
  getDate = () => {
    return new Date(Date.now()).toUTCString()
  }

  getLoggerConsole = () => winston.createLogger({
    format: this.consoleFormat,
    defaultMeta: { service: 'user-service' },
    transports: [
      new winston.transports.Console()
    ]
  })

  getLoggerFile = () => winston.createLogger({
    format: winston.format.json(),
    defaultMeta: { service: 'user-service' },
    transports: [
      new winston.transports.File({filename: './logs/app.log'})
    ]
  })
}

const loggerInstance = Logger.getInstance()
export const loggerConsole = loggerInstance.getLoggerConsole()
export const loggerFile = loggerInstance.getLoggerFile()
