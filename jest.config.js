module.exports = {
  collectCoverageFrom: [
    '**/*.ts',
    '!**/jest.config.js',
    '!**/node_modules/**',
    '!**/coverage/**',
    '!**/dist/**'
  ],
  transform: {
    '.*.ts$': 'ts-jest'
  },
  testMatch: ['**/**/*.test.ts'],
  moduleFileExtensions: ['ts', 'js', 'json'],
  preset: "ts-jest",
  coverageReporters: ['text', 'json', 'lcov'],
  coverageThreshold: {
    global: {
      branches: 95,
      functions: 100,
      lines: 100,
      statements: 100
    }
  },
  displayName: 'ci',
  resetMocks: true,
  resetModules: true,
  testEnvironment: 'node',
  testTimeout: 10000,
  verbose: true
}