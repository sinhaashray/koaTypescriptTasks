import koa from 'koa'
import Router from 'koa-router'
import { getAllRoutes } from './routeRegister'
import { loggerConsole, loggerFile } from '../logger'
const app = new koa()
const router:Router = getAllRoutes()

//logger
app.use(async (ctx, next) => {
  const startTime = Date.now()
  await next()
  const ms = Date.now() - startTime
  loggerConsole.log({
    level: 'info',
    message: `${ctx.request.method} ${ctx.request.url}`,
    status: ctx.response.status,
    'X-Response-Time': ms
  })

  loggerFile.log({
    level: 'info',
    message: `${ctx.request.method} ${ctx.request.url}`,
    status: ctx.response.status,
    header: ctx.request.header,
    'X-Response-Time': ms
  })
})

const last = <T>(arr: Array<T>) => {
  return arr[arr.length - 1]
}

const l = last([1,2,'3'])

app.use(router.routes())
app.use(router.allowedMethods())

app.listen(3001, () => {
  console.log('server listening on port 3001')
})

