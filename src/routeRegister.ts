import koaRouter from 'koa-router'
const router = new koaRouter()
import { robohashRoutes } from './routes/routesRoboHash'
import { codeforcesRoutes } from './routes/routesCodeforces' 

type responseContext = {
  response: {
    status: Number,
    message: string,
    body: ArrayBuffer | string| Array<string> | {
      message: string
    }
  }
}

const routeHandler = (route: Function) => {
  return async (ctx: responseContext, next: () => any) => {
    try{
      const response = await route(ctx)
      ctx.response.status = 200
      ctx.response.message = 'SUCCESS'
      ctx.response.body = response
    } catch(error){
      ctx.response.status = 400
      ctx.response.message = 'ERROR'
      ctx.response.body = {
        message: error.message
      }
    }
    return next()
  }
}

codeforcesRoutes.concat(robohashRoutes).map(route => {
  const { url, methods, routeFunction } = route
  router.register(url, methods, routeHandler(routeFunction))
})

export const getAllRoutes = (): koaRouter => router
