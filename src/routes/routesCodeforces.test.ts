import axios from 'axios'
import { Codeforces } from './routesCodeforces'
const codeforcesInstance = Codeforces.getInstance()
const mock = jest.spyOn(axios, 'get')

describe('Test routesCodeforces.ts', () => {
  test('getMaxRating functions returns correctly', async () => {
    const mockParameters = {
      request: {
        query: {
          handles: 'lokpati,Ashishgup,T1duS,vivo,Anushi1998,Kruti_20,CMin2021'
        }
      }
    }

    mock.mockResolvedValue({
      data: {
        result: [
          {
            handle: 'user1',
            rating: 1496
          }, {
            handle: 'user2',
            rating: 1596
          }, {
            handle: 'user3',
            rating: 1896
          }, {
            handle: 'user4',
            rating: 1996
          }, {
            handle: 'user5',
            rating: 1396
          }
        ]
      }
    })
    const response = await codeforcesInstance.getMaxRating(mockParameters)
    expect(response).toBe('user4')
  })
  test('getMaxRating throws error on bad query', async () => {
    const mockParameters = {
      request: {
        query: {
          handles: undefined
        }
      }
    }
    const wrapper = async () => codeforcesInstance.getMaxRating(mockParameters as any)
    await expect(wrapper).rejects.toEqual(Error('Invalid format of handles'))
  })
})

describe('Test getTopHandles.test.ts', () => {
  test('getTopHandles functions returns correctly', async () => {
    const mockParameters = {
      request: {
        query: {
          contest: '566'
        }
      }
    }

    mock.mockResolvedValue({
      data: {
        result: {
          rows: [
            {
              party: {
                members: [
                  { handle: 'user1_1' }
                ]
              }
            },{
              party: {
                members: [
                  { handle: 'user2_1' },
                  { handle: 'user2_2' },
                  { handle: 'user2_3' }
                ]
              }
            },{
              party: {
                members: [
                  { handle: 'user3_1' }
                ]
              }
            }
          ]
        }
      }
    })
    const response = await codeforcesInstance.getTopHandles(mockParameters)
    expect(response).toStrictEqual(['user1_1', 'user3_1'])
  })

  test('getTopHandles functions throws error on bad query', async () => {
    const mockParameters = {
      request: {
        query: {

        }
      }
    }
    const wrapper = async () => codeforcesInstance.getTopHandles(mockParameters as any)
    await expect(wrapper).rejects.toEqual(Error('Invalid format of contest'))
  })
})