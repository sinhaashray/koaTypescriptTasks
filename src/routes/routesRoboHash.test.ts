import axios from 'axios'
import { RoboHash } from './routesRoboHash'
import fs from 'fs'
const robohashInstance = RoboHash.getInstance()
const mock = jest.spyOn(axios, 'get')

describe('Test routesRoboHash.ts', () => {
  test('getOneImage returns correctly', async () => {
    const mockImageBuffer = fs.readFileSync('./src/routes/blob.txt')
    mock.mockResolvedValue({
      data: mockImageBuffer
    })
    const response = await robohashInstance.getOneImage()
    expect(response).toStrictEqual(mockImageBuffer)
  })
})