import axios from 'axios'

type IRobohash = {
  getRandomString: () => string,
  getOneImage: () => Promise<ArrayBuffer>
}


export class RoboHash implements IRobohash{

  public static instance: RoboHash | undefined = undefined

  public static getInstance() : RoboHash {
    if(this.instance !== undefined)
      return this.instance
    this.instance = new RoboHash()
    return this.instance
  }

  constructor(){}
  
  private readonly url = 'https://robohash.org/'

  getRandomString = (): string => {
    return Math.random().toString(20).substr(2, 10)
  }

  getOneImage = async ()  => {
    const generatedString = this.getRandomString()
    const response = await axios.get(`${this.url}${generatedString}`, {
      responseType: 'arraybuffer'
    })
    const imageData = Buffer.from(response.data)
    return imageData
  }
}

const roboHash = RoboHash.getInstance()

export const robohashRoutes: {url: string, methods: Array<string>, routeFunction: Function}[] = [
  {
    url: '/getimage',
    methods: ['GET'],
    routeFunction: roboHash.getOneImage
  }
]
