import axios from 'axios'

type routeContext = {
  request: {
    query: { handles: string } | { contest: string }
  }
}

interface ICodeforces{
  getMaxRating: (ctx: routeContext) => Promise<string>
  getTopHandles: (ctx: routeContext) => Promise<Array<string>>
}

export class Codeforces implements ICodeforces{

  public static instance: Codeforces | undefined = undefined

  public static getInstance(): Codeforces {
    if(this.instance !== undefined)
      return this.instance
    this.instance = new Codeforces()
    return this.instance
  }
  constructor(){}

  private readonly url = {
    userURL: 'http://codeforces.com/api/user.info?handles=',
    contestURL: 'http://codeforces.com/api/contest.standings?contestId=' 
  }

  getMaxRating = async (ctx: routeContext) => {
    const query = JSON.parse(JSON.stringify(ctx.request.query))
    const handles = query.handles
    if(!handles)
      throw new Error('Invalid format of handles')
    const handlesArray = handles.split(',')
    const userDocument = {}
    const response = await axios.get(`${this.url.userURL}${handlesArray.join(';')};`)
    const codeforcesData = response.data.result
    await codeforcesData.forEach((userElement: { handle: string | number; rating: Number }) => {
      userDocument[userElement.handle] = userElement.rating
    })
    const highestRated = Object.keys(userDocument).reduce((a, b) =>
      userDocument[a] > userDocument[b] ? a : b
    )
    return highestRated
  }

  getTopHandles = async (ctx: routeContext) => {
    const query = JSON.parse(JSON.stringify(ctx.request.query))
    const checkIfNumberRegex = /^[0-9]+$/
    if(!query.contest || !query.contest.match(checkIfNumberRegex)){
      throw new Error('Invalid format of contest')
    }
    const response = await axios.get(`${this.url.contestURL}${parseInt(query.contest)}&from=1&count=10`)
    const codeforcesData = response.data.result.rows.filter((entry: { party: { members: { handle: string }[] } }) => entry.party.members.length === 1)
    codeforcesData.forEach((element: { party: { members: { handle: string }[] } }, index: string | number, array: { [x: string]: any }) => {
      array[index] = element.party.members[0].handle
    })
    return codeforcesData
  }
}

const codeforcesInstance = Codeforces.getInstance()

export const codeforcesRoutes: {url: string, methods: Array<string>, routeFunction: Function}[] = [
  {
    url: '/maxrating',
    methods: ['GET'],
    routeFunction: codeforcesInstance.getMaxRating
  },{
    url: '/tophandles',
    methods: ['GET'],
    routeFunction: codeforcesInstance.getTopHandles
  }
]